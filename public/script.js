gsap.to("#nav", {
    // backgroundColor : "#666666",
    height : "5rem",
    duration : 0.5,
    scrollTrigger : {
        trigger : "#nav",
        scroller : "body",
        // markers : true,
        start : "top -10%",
        end : "top -15%",
        scrub : 1
    }
});

gsap.from("#services-div", {
    y: 50,
    duration: 1,
    scrollTrigger: {
      trigger: "#services-div",
      scroller: "body",
    //   markers: true,
      start: "top 50%",
      end: "top 60%",
      scrub: 5,  
    }
  });
  

let prof_title = document.querySelector("#logo");
let html = document.querySelector("#html");
let bg_btn = document.querySelector("#bg-btn");
let moon = document.querySelector("#moon");
// let sun = document.querySelector("#sun");

bg_btn.addEventListener("click", function(){
  html.classList.toggle("dark");
  if(html.classList.contains("dark")){
    moon.style.display = "none";
    sun.style.display = "block";
  }else{
    moon.style.display = "block";
    sun.style.display = "none";
  }
});

prof_title.addEventListener("click", function(){
    window.scrollTo(0, 0);

    history.replaceState(null, document.title, window.location.pathname); // is just telling the browser, "Okay, keep the webpage title the same and make the address bar forget about that #skills part.
});