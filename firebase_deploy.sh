#!/bin/bash

# Log in to Firebase (if not already logged in)
firebase login:ci

# Deploy to Firebase Hosting
firebase deploy --token "$FIREBASE_TOKEN"
